## Requirements on the host
- Raspberry Pi
- [Arch Linux ARM](https://archlinuxarm.org)
- [Python 2.x](https://wiki.archlinux.org/index.php/Python#Python_2)
- Add your public SSH key to `/home/alarm/.ssh/authorized_keys`

## Run it!
- Clone this repo
- Add `moo.moo` entry to your `/etc/hosts`
- Run `ansible-playbook provision.yml`
- Input `alarm` user password
- Input vault password
